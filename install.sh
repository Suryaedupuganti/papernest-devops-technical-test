#!/bin/bash

# Docker CE Install
sudo amazon-linux-extras install -y docker

# To start the docker service 
sudo service docker start

#give the usermod permissions to the docker group
sudo usermod -a -G docker ec2-user

# Make docker auto-start
sudo chkconfig docker on

# docker version
sudo docker version

# docker compose
sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

# Fix permissions after download:
sudo chmod +x /usr/local/bin/docker-compose

# Verify success:
docker-compose version
