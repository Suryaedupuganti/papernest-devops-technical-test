# papernest-devops-technical-test

The project is based on technical test document **[DevOps Technical Test.pdf](./DevOps Technical Test.pdf)**.

## Result

Application can be accessed through: [34.245.69.203:8080](34.245.69.203:8080)

## Cloud formation template

Cloud formation template creates a dedicated VPC with one public subnet, internet gateway and route-table associations.
It also creates EC2 instance in the public subnet with a custom security group.

## Install sh

The script [install.sh](./install.sh) installs docker and docker-compose in EC2 instance.

## mykeypair.ppk

key pair to access the instance.

